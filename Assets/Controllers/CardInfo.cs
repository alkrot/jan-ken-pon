using System;
using UnityEngine;
using UnityEngine.UI;

public class CardInfo : MonoBehaviour
{
    [SerializeField] private Card selfCard;
    [SerializeField] private Image Logo;


    public void HideCardInfo(Card card)
    {
        selfCard = card;
        Logo.sprite = card.Shirt;
    }

    public void ShowInfo(Card card)
    {
        selfCard = card;
        Logo.preserveAspect = true;
        Logo.sprite = card.Logo;
    }

    public Card GetCard()
    {
        return selfCard;
    }
}
