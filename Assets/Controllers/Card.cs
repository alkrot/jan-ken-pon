﻿using UnityEngine;

public struct Card
{
    private CardState cardState;
    private Sprite logo;
    private Sprite shirt;

    public Card(CardState _cardState, string _logo)
    {
        cardState = _cardState;
        logo = Resources.Load<Sprite>(_logo);
        shirt = Resources.Load<Sprite>("Sprites/Cards/faceUp");
    }

    public Sprite Logo
    {
        get { return logo; }
    }

    public Sprite Shirt
    {
        get { return shirt; }
    }

    public CardState CardState
    {
        get { return cardState; }
    }
}
