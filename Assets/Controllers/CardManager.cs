using UnityEngine;

public class CardManager : MonoBehaviour
{
    public void Awake()
    {
        CardManagerInfo.AllCards.Add(new Card(CardState.Rock, "Sprites/Cards/Rock"));
        CardManagerInfo.AllCards.Add(new Card(CardState.Scissors, "Sprites/Cards/Scissors"));
        CardManagerInfo.AllCards.Add(new Card(CardState.Paper, "Sprites/Cards/Paper"));
    }
}
