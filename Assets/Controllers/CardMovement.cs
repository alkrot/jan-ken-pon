using UnityEngine;
using UnityEngine.EventSystems;

public class CardMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Camera mainCamera;
    private Vector3 offset;
    Transform defaultParent, defaultTempCardParent;
    private GameObject tempCardGO;
    private GameManager gameManager;
    private bool IsDragable;

    void Awake()
    {
        mainCamera = Camera.main;
        tempCardGO = GameObject.Find("TempCardGO");
        gameManager = FindObjectOfType<GameManager>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        offset = transform.position - mainCamera.ScreenToWorldPoint(eventData.position);

        defaultParent = defaultTempCardParent = transform.parent;

        IsDragable = defaultParent.GetComponent<DropPlace>().IsFieldType(FieldType.SelfHand) && gameManager.IsPlayer();

        if (!IsDragable) return;

        tempCardGO.transform.SetParent(defaultParent);
        tempCardGO.transform.SetSiblingIndex(transform.GetSiblingIndex());

        transform.SetParent(defaultParent.parent);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!IsDragable) return;

        var newPos = mainCamera.ScreenToWorldPoint(eventData.position);
        transform.position = newPos + offset;

        if (tempCardGO.transform.parent != defaultTempCardParent)
            tempCardGO.transform.SetParent(defaultTempCardParent, false);

        CheckPosition();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!IsDragable) return;

        transform.SetParent(defaultParent);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        transform.SetSiblingIndex(tempCardGO.transform.GetSiblingIndex());
        tempCardGO.transform.SetParent(GameObject.Find("Canvas").transform);
        tempCardGO.transform.localPosition = new Vector3(2340, 0);
    }

    private void CheckPosition()
    {
        int newIndex = defaultTempCardParent.childCount;

        for(int i = 0; i < defaultTempCardParent.childCount; i++)
        {
            if(transform.position.x < defaultTempCardParent.GetChild(i).position.x)
            {
                newIndex = i;
                if (tempCardGO.transform.GetSiblingIndex() < newIndex)
                    newIndex--;
                break;
            }
        }

        tempCardGO.transform.SetSiblingIndex(newIndex);
    }

    public Transform GetDefaultParent()
    {
        return defaultParent;
    }

    public Transform GetDefaultTempParent()
    {
        return defaultTempCardParent;
    }

    public void SetDefaultParent(Transform transform)
    {
        defaultParent = transform;
    }

    public void SetDefaulTempParent(Transform transform)
    {
        defaultTempCardParent = transform;
    }

    public GameManager GameManager()
    {
        return gameManager;
    }
}
