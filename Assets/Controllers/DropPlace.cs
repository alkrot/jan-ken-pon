using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropPlace : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private FieldType fieldType;

    public FieldType GetFieldType()
    {
        return fieldType;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (fieldType != FieldType.SelfField) return;

        CardMovement card = eventData.pointerDrag.GetComponent<CardMovement>();

        if (card && card.GameManager().IsEnoughCard())
        {
            card.GameManager().TurnHandToFieldPlayer(card.GetComponent<CardInfo>());
            card.SetDefaultParent(transform);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null || fieldType == FieldType.EnemyField || fieldType == FieldType.EnemyHand) return;

        CardMovement card = eventData.pointerDrag.GetComponent<CardMovement>();

        if (card)
            card.SetDefaulTempParent(transform);
    }

    public bool IsFieldType(FieldType value)
    {
        return fieldType == value;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null) return;
        
        CardMovement card = eventData.pointerDrag.GetComponent<CardMovement>();

        if (card && card.GetDefaultTempParent() == transform)
            card.SetDefaulTempParent(card.GetDefaultParent());
    }
}
