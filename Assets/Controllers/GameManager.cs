using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Game CurrentGame;
    [SerializeField] private Transform enemyHand;
    [SerializeField] private Transform playerHand;
    [SerializeField] private Transform enemyField;
    [SerializeField] private Transform playerField;
    [SerializeField] private GameObject cardPref;

    [SerializeField] private Text playerPointText;
    [SerializeField] private Text enemyPointText;
    [SerializeField] private Text roundText;

    [SerializeField] private Text EndTurnText;
    [SerializeField] private Button EndTurnButton;

    private const int maxCardInField = 1;

    private int round = 1;
    private int whoTurn;
    private int turnTime;

    private int playerPoint, enemyPoint = 0;
    private readonly List<CardInfo> playerHandCard = new List<CardInfo>();
    private readonly List<CardInfo> playerFieldCards = new List<CardInfo>();
    private readonly List<CardInfo> enemyHandCard = new List<CardInfo>();
    private readonly List<CardInfo> enemyFieldCards = new List<CardInfo>();

    public bool IsEnoughCard()
    {
        return playerFieldCards.Count != maxCardInField;
    }

    private void Start()
    {
        whoTurn = 0;

        CurrentGame = new Game();
        GiveHandCards(CurrentGame.Deck, enemyHand);
        GiveHandCards(CurrentGame.Deck, playerHand);
        StartCoroutine(Turn());
    }

    void GiveHandCards(List<Card> deck, Transform hand)
    {
        int i = 0;
        while (i++ < 3) GiveCardToHand(deck, hand);
    }

    void GiveCardToHand(List<Card> deck, Transform hand)
    {
        if (deck.Count == 0) return;
        Card card = deck[0];

        GameObject cardGO = Instantiate(cardPref, hand, false);
        CardInfo cardInfo = cardGO.GetComponent<CardInfo>();

        if (hand == enemyHand)
        {
            cardInfo.HideCardInfo(card);
            enemyHandCard.Add(cardInfo);
        }
        else
        {
            cardInfo.ShowInfo(card);
            playerHandCard.Add(cardInfo);
        }

        deck.Remove(card);
    }

    public void TurnHandToFieldPlayer(CardInfo cardInfo)
    {
        playerHandCard.Remove(cardInfo);
        playerFieldCards.Add(cardInfo);
    }

    public void TurnHandToFieldEnemy(CardInfo cardInfo)
    {
        enemyHandCard.Remove(cardInfo);
        enemyFieldCards.Add(cardInfo);
    }


    public void ChangeTurn()
    {
        StopAllCoroutines();
        whoTurn++;
        EndTurnButton.interactable = IsPlayer();
        StartCoroutine(Turn());
    }

    public bool IsPlayer()
    {
        return whoTurn % 2 == 0;
    }

    private IEnumerator Turn()
    {
        if (enemyFieldCards.Count == 1 && playerFieldCards.Count == 1)
        {
            EndTurnButton.interactable = false;
            yield return new WaitForSeconds(5);
            CheckWin(playerFieldCards[0], enemyFieldCards[0]);
            EndTurnButton.interactable = true;
        }

        turnTime = 30;
        EndTurnText.text = turnTime.ToString();

        if (IsPlayer())
        {
            while (turnTime-- > 0)
            {
                EndTurnText.text = turnTime.ToString();
                yield return new WaitForSeconds(1);
            }
        }
        else
        {
            while (turnTime-- > 27)
            {
                EndTurnText.text = turnTime.ToString();
                yield return new WaitForSeconds(1);
            }

            if (enemyHandCard.Count > 0) EnemyTurn(enemyHandCard);
        }

        ChangeTurn();
    }

    private void CheckWin(CardInfo playerCardInfo, CardInfo enemyCardInfo)
    {
        var playerCard = playerCardInfo.GetCard();
        var enemyCard = enemyCardInfo.GetCard();

        if (playerCard.CardState == enemyCard.CardState)
        {
            DropFieldCards(playerCardInfo, enemyCardInfo);
        }
        else
        {

            var playerWin = playerCard.CardState == CardState.Rock && enemyCard.CardState == CardState.Scissors
                || playerCard.CardState == CardState.Scissors && enemyCard.CardState == CardState.Paper
                || playerCard.CardState == CardState.Paper && enemyCard.CardState == CardState.Rock;

            if (playerWin)
            {
                playerPoint++;
                playerPointText.text = playerPoint.ToString();
            }
            else
            {
                enemyPoint++;
                enemyPointText.text = enemyPoint.ToString();
            }

            DropFieldCards(playerCardInfo, enemyCardInfo);
        }

        if(playerHandCard.Count == 0 && enemyHandCard.Count == 0)
        {
            GiveHandCards(CurrentGame.Deck, playerHand);
            GiveHandCards(CurrentGame.Deck, enemyHand);

            round++;
            roundText.text = round.ToString();
        }
    }

    private void DropFieldCards(CardInfo playerCardInfo, CardInfo enemyCardInfo)
    {
        playerFieldCards.Remove(playerCardInfo);
        enemyFieldCards.Remove(enemyCardInfo);

        var playerCard = playerField.GetChild(0);
        var enemyCard = enemyField.GetChild(0);

        playerField.DetachChildren();
        enemyField.DetachChildren();

        Destroy(playerCard.gameObject);
        Destroy(enemyCard.gameObject);
    }

    void EnemyTurn(List<CardInfo> cardInfos)
    {
        int count = Random.Range(1, cardInfos.Count);

        for (int i = 0; i < count; i++)
        {
            if (enemyFieldCards.Count == maxCardInField) return;

            cardInfos[0].ShowInfo(cardInfos[0].GetCard());
            cardInfos[0].transform.SetParent(enemyField);

            TurnHandToFieldEnemy(cardInfos[0]);
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
