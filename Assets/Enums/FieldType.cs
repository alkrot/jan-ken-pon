﻿public enum FieldType
{
    SelfHand = 0,
    SelfField = 1,
    EnemyField = 2,
    EnemyHand = 3
}