﻿using System.Collections.Generic;
using UnityEngine;

public class Game
{
    public List<Card> Deck;

    private const int cardCount = 30;

    public Game()
    {
        Deck = GiveDeckCard();
    }

    List<Card> GiveDeckCard()
    {
        List<Card> list = new List<Card>();
        for (int i = 0; i < cardCount; i++)
        {
            list.Add(CardManagerInfo.AllCards[Random.Range(0, CardManagerInfo.AllCards.Count)]);
        }

        return list;
    }
}